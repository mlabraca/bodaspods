# BodasPods

[![CI Status](https://img.shields.io/travis/Miguel Angel Labraca/BodasPods.svg?style=flat)](https://travis-ci.org/Miguel Angel Labraca/BodasPods)
[![Version](https://img.shields.io/cocoapods/v/BodasPods.svg?style=flat)](https://cocoapods.org/pods/BodasPods)
[![License](https://img.shields.io/cocoapods/l/BodasPods.svg?style=flat)](https://cocoapods.org/pods/BodasPods)
[![Platform](https://img.shields.io/cocoapods/p/BodasPods.svg?style=flat)](https://cocoapods.org/pods/BodasPods)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

BodasPods is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'BodasPods'
```

## Author

Miguel Angel Labraca, mlabraca@bodas.net

## License

BodasPods is available under the MIT license. See the LICENSE file for more info.