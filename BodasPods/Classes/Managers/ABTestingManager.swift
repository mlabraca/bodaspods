//
//  ABTestingManager.swift
//  Wedshoots
//
//  Created by Miguel Angel Labraca on 11/05/2018.
//  Copyright © 2018 Egle Systems S.L. All rights reserved.
//

import UIKit

@objc(WSABTestClass)
public class ABTestClass: NSObject {
    
    @objc
    public static let testGDPR: DataTest = DataTest(
        name: "test_gdpr",
        defaultValue: false as NSObject,
        valueType: Bool.self
    )
    
    @objc
    public static let testExperimental: DataTest = DataTest(
        name: "test_experimental",
        defaultValue: false as NSObject,
        valueType: Bool.self
    )
    
    override init() {}
    
    @objc
    public static var tests: [String : NSObject] {
        var tests: [String : NSObject] = [:]
        tests[ABTestClass.testGDPR.name] = ABTestClass.testGDPR.defaultValue
        tests[ABTestClass.testExperimental.name] = ABTestClass.testExperimental.defaultValue
        return tests
    }
}

import FirebaseCore
import FirebaseRemoteConfig

@objc(WSDataTest)
public class DataTest: NSObject {
    let name: String
    let defaultValue: NSObject?
    let valueType: Any.Type
    
    init(name: String, defaultValue: NSObject?, valueType: Any.Type) {
        self.name = name
        self.defaultValue = defaultValue
        self.valueType = valueType
    }
}

@objc(WSABTestingManager)
public class ABTestingManager: NSObject {
    
    @objc
    public static let sharedInstance = ABTestingManager()
    private let targetName: String
    private var remoteConfig: RemoteConfig!
    
    override init() {
        let targetName = Bundle.main.infoDictionary?["CFBundleName"] as? String ?? ""
        self.targetName = targetName
    }
    
    @objc
    public func configure(withABTests abTests: [String: NSObject]) {
    //public func configure(withABTests abTests: ABTest) {
        // Use Firebase library to configure APIs
        if FirebaseApp.app() == nil {
            FirebaseApp.configure()
        }
        remoteConfig = RemoteConfig.remoteConfig()
        //let tests: [String : NSObject] = abTest.tests
        //remoteConfig.setDefaults(tests)
        remoteConfig.setDefaults(abTests)
        remoteConfig.fetch { (status, error) in
            if status == .success {
                self.remoteConfig.activateFetched()
            }
        }
    }
    
    // MARK: - AB test calling functions
    
//    func getValue<T>(fromTest abTest: ABTest) -> T? {
//        if let value = remoteConfig[abTest.name] as? T {
//            return value
//        } else {
//            return nil
//        }
//    }
    
    @objc
    //public func getValue(fromTest abTest: ABTest) -> NSObject? {
    public func getValue(fromTest abTest: DataTest) -> NSObject? {
    
        switch abTest.valueType {
        case is Bool.Type:
            let boolValue = remoteConfig[abTest.name].boolValue as NSObject
            return boolValue
            //return remoteConfig[abTest.name].boolValue as NSObject
        case is String.Type:
            return remoteConfig[abTest.name].stringValue as NSObject? ?? nil
        case is Data.Type:
            return remoteConfig[abTest.name].dataValue as NSObject
        case is NSNumber.Type:
            return remoteConfig[abTest.name].numberValue
        default:
            return nil
        }
    }
    
    @objc
    public func getBoolValue(fromTest abTest: DataTest) -> Bool {
        let value = remoteConfig[abTest.name].boolValue
        return value
    }
    
    @objc
    public func getStringValue(fromTest abTest: DataTest) -> String {
        let value = remoteConfig[abTest.name].stringValue!
        return value
    }
    
    @objc
    public func getDataValue(fromTest abTest: DataTest) -> Data {
        let value = remoteConfig[abTest.name].dataValue
        return value
    }
    
    @objc
    public func getNumberValue(fromTest abTest: DataTest) -> NSNumber {
        let value = remoteConfig[abTest.name].numberValue!
        return value
    }
}


@objc(WSABTest)
public enum ABTest: Int {
    case testDefault
    case gdpr
    
    public var name: String {
        switch self {
        case .testDefault: return "test_default"
        case .gdpr: return "is_gdpr"
        }
    }
    
    public var defaultValue: NSObject {
        switch self {
        case .testDefault: return "" as NSObject
        case .gdpr: return true as NSObject
        }
    }
    
    public var valueType: Any.Type {
        switch self {
        case .testDefault: return String.self
        case .gdpr: return Bool.self
        }
    }
    
    public var tests: [String: NSObject] {
        var tests: [String: NSObject] = [:]
        tests[ABTest.testDefault.name] = ABTest.testDefault.defaultValue
        tests[ABTest.gdpr.name] = ABTest.gdpr.defaultValue
        return tests
    }
}
